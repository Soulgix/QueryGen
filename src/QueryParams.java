import java.util.*;

public class QueryParams {
    private String tipoQuery;
    private List<String> nomeTabella;
    private List<String> campi;
    private List<String> valori;
    private List<String> tipoValori;
    private List<String> campiIdentificativiTabelle;
    private List<String> valoriIdentificativiTabelle;
    private List<String> campiIdentificativi;
    private List<String> valoriIdentificativi;
    private List<String> tipoValoriId;
    private String orderBy;


    private List<String> campiLazy;

    private LinkedHashMap<String, String> valoriLazyOrdinati;
    
    private Map<String, OggettoValori> valoriLazyMappa;

    public void addCampoLazy(String campo) {

        campiLazy.add(campo);
    }
    
    public void addValoreLazyOrdinato(String campo, String tipo) {
        
        valoriLazyOrdinati.put(campo, tipo);
    }

    public void addValoreLazyMappa(String campo, OggettoValori oggettoValori) {

        valoriLazyMappa.put(campo, oggettoValori);
    }
    

    public void addCampo(String campo) {
        campi.add(campo);
    }


    public List<String> getCampiLazy() {
        return campiLazy;
    }

    public LinkedHashMap<String, String> getValoriLazyOrdinati() {
        return valoriLazyOrdinati;
    }

    public Map<String, OggettoValori> getValoriLazyMappa() {
        return valoriLazyMappa;
    }

    public void addNomeTabella(String nomeTabellaEle) {
        nomeTabella.add(nomeTabellaEle);
    }

    public void addValore(String valore) {
        valori.add(valore);
    }

    public void addTipoCampo(String tipoCampo) {
        tipoValori.add(tipoCampo);
    }

    public void addCampoIdTab(String campoIdTab) {
        campiIdentificativiTabelle.add(campoIdTab);
    }

    public void addValoreIdTab(String valIdTab) {
        valoriIdentificativiTabelle.add(valIdTab);
    }

    public void addCampoId(String campoId) {
        campiIdentificativi.add(campoId);
    }

    public void addValoreId(String valoreId) {
        valoriIdentificativi.add(valoreId);
    }

    public void addTipoValoreId(String tipoCampoId) {
        tipoValoriId.add(tipoCampoId);
    }

    public void initializeLists() {
        nomeTabella = new ArrayList<>();
        campi = new ArrayList<>();
        valori = new ArrayList<>();
        tipoValori = new ArrayList<>();
        campiIdentificativiTabelle = new ArrayList<>();
        valoriIdentificativiTabelle = new ArrayList<>();
        campiIdentificativi = new ArrayList<>();
        valoriIdentificativi = new ArrayList<>();
        tipoValoriId = new ArrayList<>();
        valoriLazyOrdinati = new LinkedHashMap<>();
        campiLazy = new ArrayList<>();
        valoriLazyMappa = new HashMap<>();
    }

    public String getTipoQuery() {
        return tipoQuery;
    }

    public void setTipoQuery(String tipoQuery) {
        this.tipoQuery = tipoQuery;
    }

    public List<String> getNomeTabella() {
        return nomeTabella;
    }

    public void setNomeTabella(List<String> nomeTabella) {
        this.nomeTabella = nomeTabella;
    }

    public List<String> getCampi() {
        return campi;
    }

    public void setCampi(List<String> campi) {
        this.campi = campi;
    }

    public List<String> getValori() {
        return valori;
    }

    public void setValori(List<String> valori) {
        this.valori = valori;
    }

    public List<String> getTipoValori() {
        return tipoValori;
    }

    public void setTipoValori(List<String> tipoValori) {
        this.tipoValori = tipoValori;
    }

    public List<String> getCampiIdentificativiTabelle() {
        return campiIdentificativiTabelle;
    }

    public void setCampiIdentificativiTabelle(List<String> campiIdentificativiTabelle) {
        this.campiIdentificativiTabelle = campiIdentificativiTabelle;
    }

    public List<String> getValoriIdentificativiTabelle() {
        return valoriIdentificativiTabelle;
    }

    public void setValoriIdentificativiTabelle(List<String> valoriIdentificativiTabelle) {
        this.valoriIdentificativiTabelle = valoriIdentificativiTabelle;
    }

    public List<String> getCampiIdentificativi() {
        return campiIdentificativi;
    }

    public void setCampiIdentificativi(List<String> campiIdentificativi) {
        this.campiIdentificativi = campiIdentificativi;
    }

    public List<String> getValoriIdentificativi() {
        return valoriIdentificativi;
    }

    public void setValoriIdentificativi(List<String> valoriIdentificativi) {
        this.valoriIdentificativi = valoriIdentificativi;
    }

    public List<String> getTipoValoriId() {
        return tipoValoriId;
    }

    public void setTipoValoriId(List<String> tipoValoriId) {
        this.tipoValoriId = tipoValoriId;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
    public void convertStringsToUpper() {
        tipoQuery =  tipoQuery.toUpperCase();
        orderBy = orderBy.toUpperCase();
    }

    public void convertArraysToUpper() {
        convertArrayToUpper(nomeTabella);
        convertArrayToUpper(campi);
        convertArrayToUpper(valori);
        convertArrayToUpper(tipoValori);
        convertArrayToUpper(campiIdentificativiTabelle);
        convertArrayToUpper(valoriIdentificativiTabelle);
        convertArrayToUpper(campiIdentificativi);
        convertArrayToUpper(valoriIdentificativi);
        convertArrayToUpper(tipoValoriId);
    }

    // Metodo per convertire un array in maiuscolo
    private void convertArrayToUpper(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            String element = list.get(i);
            if (element != null) {
                list.set(i, element.toUpperCase());
            }
        }
    }

}
