public class OggettoValori {

    private String valore;

    private String type;

    public OggettoValori(String valore, String type) {
        this.valore = valore;
        this.type = type;
    }

    public String getValore() {
        return valore;
    }

    public void setValore(String valore) {
        this.valore = valore;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
