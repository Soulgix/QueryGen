import java.sql.Types;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Date now = new Date();
        QueryParams queryParams = new QueryParams();
//        queryParams.setTipoQuery(QueryElements.QUERY_TYPE_SELECT);
////        queryParams.setTipoQuery(QueryElements.QUERY_TYPE_UPDATE);
////        queryParams.setTipoQuery(QueryElements.QUERY_TYPE_INSERT);
        queryParams.initializeLists();
//        queryParams.addNomeTabella("ORT_T_ALLEGATI_RICHIESTA.BB");
//        queryParams.addNomeTabella("TABELLA2.CC.J");
//        queryParams.addNomeTabella("TABELLA3.DD.LJ");
//        queryParams.addCampo("BB.DATA_FINE");
//        queryParams.addCampo("BB.EXT_ID_UTENTE_AGGIORNAMENTO");
//        queryParams.addValore(now.toString());
//        queryParams.addValore("NULL");
//        queryParams.addTipoCampo(QueryElements.Type_TIMESTAMP);
//        queryParams.addTipoCampo("NULL");
//        queryParams.addCampoIdTab("CC.DATA_FINE");
//        queryParams.addCampoIdTab("CC.secondocampo");
//        queryParams.addCampoIdTab("DD.Palla");
//        queryParams.addValoreIdTab("DD.DATA_FINE");
//        queryParams.addValoreIdTab("hh.secondovalore");
//        queryParams.addValoreIdTab("bb.palla");
//        queryParams.addCampoId("BB.DATA_FINE");
//        queryParams.addCampoId("BB.ID_PROCEDIMENTO_OGGETTO");
//        queryParams.addCampoId("BB.ID_ALTRI_ALLEGATI");
//        queryParams.addCampoId("BB.PALLA.TAB");
//        queryParams.addValoreId("NULL");
//        queryParams.addValoreId("12");
//        queryParams.addValoreId("12");
//        queryParams.addValoreId("CC.PALLA");
//        queryParams.addTipoCampoId("NULL");
//        queryParams.addTipoCampoId(QueryElements.Type_NUMERIC);
//        queryParams.addTipoCampoId(QueryElements.Type_NUMERIC);
//        queryParams.addTipoCampoId("NULL");
//        queryParams.setOrderBy("something");
//        updateCampoGenerico( queryParams );
        queryParams.addValore("565");
        queryParams.addValore("ciao1");
        queryParams.addValore("ciao2");
        queryParams.addTipoCampo(QueryElements.Type_NUMERIC);
        queryParams.addTipoCampo(QueryElements.Type_TIMESTAMP);
        queryParams.addTipoCampo(QueryElements.Type_VARCHAR);
        queryParams.addValoreId("ciao5");
        queryParams.addValoreId("ciao6");
        queryParams.addTipoValoreId(QueryElements.Type_VARCHAR);
        queryParams.addTipoValoreId(QueryElements.Type_VARCHAR);
        queryParams.getValoriLazyOrdinati().put("palla", QueryElements.Type_VARCHAR);
        queryParams.getValoriLazyOrdinati().put("pillo", QueryElements.Type_VARCHAR);
        queryParams.getValoriLazyOrdinati().put("polla", QueryElements.Type_VARCHAR);
        queryGeneratorLazyNoAlarmStillInBed("SELECT c.nome_cliente, o.numero_ordine, p.nome_prodotto\n" +
                "FROM clienti c\n" +
                "JOIN ordini o ON c.id_cliente = o.id_cliente\n" +
                "JOIN dettagli_ordine d ON o.numero_ordine = d.numero_ordine\n" +
                "JOIN prodotti p ON d.id_prodotto = :p.id_prodotto\n" +
                "WHERE o.stato = ':Consegnato' AND p.prezzo = :10.0\n" +
                "ORDER BY c.nome_cliente, o.data_ordine DESC;\n", queryParams,  queryParams.getValoriLazyOrdinati(), null);
//        queryGeneratorLazy("DELETE FROM tabella cc\n" +
//                "WHERE cc.condizione1 = :valore_condizione1 AND cc.condizione2 = :valore_condizione2;\n", queryParams);
        String proiva = "  ciao domani   ecco ";
        List<String> split = Arrays.asList(proiva.trim().split("\\s+"));
        System.out.println(split);

    }

    public static void queryGenerator(QueryParams queryParams)
    {
        queryParams.convertStringsToUpper();
        queryParams.convertArraysToUpper();

        StringBuilder QUERY = new StringBuilder();
        int index = 0;
        int indexx = 0;
        switch (queryParams.getTipoQuery()){
            case "UPDATE":
                QUERY.append(" UPDATE                             \n" + "   ").append(
                        queryParams.getNomeTabella().get(0).split("\\.")[0]).append(
                                "  ").append(queryParams.getNomeTabella().get(0).split("\\.")[1]).append(
                                        " \n").append("  SET          \n");
                for (String campo: queryParams.getCampi()) {
                    if (queryParams.getValori().get(index).equals("NULL")) {
                        QUERY.append(campo).append(" =  NULL");
                    } else {
                        QUERY.append(campo).append(" = :").append(campo);
                    }
                    index++;
                    if (queryParams.getCampi().size() != index) {
                        QUERY.append(",  \n");}
                    else {
                        QUERY.append("  \n");}

                }
                WHEREBlock(QUERY, queryParams, indexx);
                break;
            case "DELETE":
                QUERY.append(" DELETE FROM                            \n" + "   ").append(
                        queryParams.getNomeTabella().get(0).split("\\.")[0]).append(
                        "  ").append(queryParams.getNomeTabella().get(0).split("\\.")[1]);
                WHEREBlock(QUERY, queryParams, indexx);
                break;
            case "SELECT":
                QUERY.append(" SELECT                                                 	  \n");
                for (String campo: queryParams.getCampi()) {
                    QUERY.append(campo).append("   \n");
                }
                QUERY.append(" FROM   \n   ").append(queryParams.getNomeTabella().get(0).split("\\.")[0]).append("  ").append(queryParams.getNomeTabella().get(0).split("\\.")[1]).append(" \n");
                for (int i = 1; i < queryParams.getNomeTabella().size(); i++) {
                    int palla = queryParams.getNomeTabella().get(i).split("\\.")[2].length();
                    String dssfd = queryParams.getNomeTabella().get(i).split("\\.")[2];
                    boolean dssghjfd = queryParams.getNomeTabella().get(i).split("\\.")[2].equals("LJ");
                    if (queryParams.getNomeTabella().get(i).split("\\.")[2].length() <= 3 && queryParams.getNomeTabella().get(i).split("\\.")[2].equals("LJ")) {
                        QUERY.append(" LEFT JOIN  ");
                    } else {
                        QUERY.append(" JOIN  ");
                    }
                    QUERY.append(queryParams.getNomeTabella().get(i).split("\\.")[0]).append("  ").append(queryParams.getNomeTabella().get(i).split("\\.")[1]).append(" ON ");
                    index=0;
                    for (String collegamento: queryParams.getCampiIdentificativiTabelle()) {
                        if (collegamento.split("\\.")[0].equals(queryParams.getNomeTabella().get(i).split("\\.")[1])) {
                            QUERY.append(" ").append(collegamento).append(" = ").append(queryParams.getValoriIdentificativiTabelle().get(index)).append("  \n");
                        }
                        if (index +1 != queryParams.getCampiIdentificativiTabelle().size() && collegamento.split("\\.")[0].equals(queryParams.getNomeTabella().get(i).split("\\.")[1]) && queryParams.getCampiIdentificativiTabelle().get(index +1).split("\\.")[0].equals(queryParams.getNomeTabella().get(i).split("\\.")[1])) {
                            QUERY.append(" and ");
                        }index++;
                    }
                }
                WHEREBlock(QUERY, queryParams, indexx);
                break;
            case "INSERT":
                QUERY.append(" INSERT INTO                            \n" + "   ").append(
                        queryParams.getNomeTabella().get(0).split("\\.")[0]).append(
                        " \n").append("  (  \n");
                int indexInsert = 1;
                for (String campo: queryParams.getCampi()) {
                    QUERY.append(campo);
                    QUERY.append(indexInsert != queryParams.getCampi().size() ? ",  \n" : " )  \n");
                    indexInsert++;
                }
                QUERY.append("  VALUES (  \n");

//                NO VA NEL CICLO SOPRA-------------------------------------
                int indexInsertValori = 0;
                for (String campo: queryParams.getCampi()) {
                    QUERY.append(
                            (queryParams.getTipoValori().get(indexInsertValori).equals(QueryElements.Type_TIMESTAMP)) ? " SYSDATE " :
                            ((queryParams.getValori().get(indexInsertValori).equals("NULL")) ? " NULL "
                                    : ":" + campo)
                    );
                    QUERY.append(indexInsertValori != (queryParams.getCampi().size() -1 ) ? ",  \n" : " )  \n");
                    indexInsertValori++;
                }
                break;

        }

        if (queryParams.getOrderBy() != null && !queryParams.getOrderBy().isEmpty()) {
            QUERY.append("  ORDER_BY ").append(queryParams.getOrderBy()).append("          \n");}


        Map<Mykey, String> mapParameterSource = new HashMap<>();

            int indexxx = 0;
            int indexxxx = 0;
            if (!queryParams.getTipoQuery().equals("SELECT")) {
                for (String campo: queryParams.getCampi()) {
                    if (!queryParams.getTipoValori().get(indexxx).equals("NULL")) {
                        mapParameterSource.put(new Mykey(campo, convertType(queryParams.getTipoValori().get(indexxx), queryParams.getValori().get(indexxx)), checkType(queryParams.getTipoValori().get(indexxx))), "");
                    }
                    indexxx++;
                }
            }

            for (String campoId: queryParams.getCampiIdentificativi()) {
                if (!queryParams.getTipoValoriId().get(indexxxx).equals("NULL")) {
                    mapParameterSource.put(new Mykey(campoId, convertType(queryParams.getTipoValoriId().get(indexxxx), queryParams.getValoriIdentificativi().get(indexxxx)), checkType(queryParams.getTipoValoriId().get(indexxxx))), "");
                }
                indexxxx++;
            }
            System.out.println(QUERY);
        for (Mykey key: mapParameterSource.keySet()) {
            System.out.println("mapParam" + key.key1);
        }

        }

    public static int checkType(String arrayType) {
        switch (arrayType) {
            case QueryElements.Type_NUMERIC:
                return Types.NUMERIC;
            case QueryElements.Type_VARCHAR:
                return Types.VARCHAR;
            case QueryElements.Type_TIMESTAMP:
                return Types.TIMESTAMP;
            default:
                return 0;
        }
    }
    public static Object convertType(String arrayType, String stringa) {
        switch (arrayType) {
            case QueryElements.Type_NUMERIC:
                return Long.parseLong(stringa);
            case QueryElements.Type_VARCHAR:
                return stringa;
            case QueryElements.Type_TIMESTAMP:
                return new Date();
            default:
                return new Object();
        }
    }

    public static StringBuilder WHEREBlock (StringBuilder QUERY, QueryParams queryParams, int indexx) {
        QUERY.append("  WHERE                                                  	  \n");
        for (String campoId: queryParams.getCampiIdentificativi()) {
            if (queryParams.getValoriIdentificativi().get(indexx).equals("NULL")) {
                QUERY.append(campoId).append(" IS NULL");
            } else if (queryParams.getValoriIdentificativi().get(indexx).split("\\.").length >= 3) {
                QUERY.append(campoId).append(" = ").append(
                        queryParams.getValoriIdentificativi().get(indexx).split("\\.")[0]).append(
                        ".").append(queryParams.getValoriIdentificativi().get(indexx).split("\\.")[1]);
            } else {
                QUERY.append(campoId).append(" = :").append(campoId);
            }
            indexx++;
            if (queryParams.getCampiIdentificativi().size() != indexx) {
                QUERY.append("  and  \n");}
            else {
                QUERY.append("  \n");}

        }
        return QUERY;
    }

    public static void queryGeneratorLazy(String queryTotale, QueryParams queryParams) {
        String modified = queryTotale.replaceAll("\"([^\"]*)\"", "\"$1\"")
                .replaceAll("'([^']*)'", "'$1'")
                .replaceAll("[\\n+\\s]", " ");

        String[] splitArray = modified.trim().split("\\s+");
        List<String> split = Arrays.asList(splitArray);
        String identificatore = split.get(0);
        int indexWhere = split.indexOf("WHERE");
        switch (identificatore) {
            case "SELECT":
                queryParams.setTipoQuery(QueryElements.QUERY_TYPE_SELECT);
                int foundIndex = split.indexOf("FROM");
                queryParams.addNomeTabella(split.get(foundIndex + 1) + "." + split.get(foundIndex + 2));
                for (int i = 1; i < foundIndex; i++) {
                    queryParams.addCampo(split.get(i));
                }
                if (!split.get(foundIndex+3).equals("WHERE")) {
                    queryGeneratorLazyJoin(queryParams, split, foundIndex+3, indexWhere);
                }

                queryParams.setOrderBy(split.get(split.indexOf("ORDER") + 2 ));
                break;
            case "UPDATE":
                queryParams.setTipoQuery(QueryElements.QUERY_TYPE_UPDATE);
                queryParams.addNomeTabella(split.get(1)+"."+split.get(2));
                for (int i = 4; i < indexWhere; i+=3) {
                    queryParams.addCampo(split.get(i).replaceAll("[,()]", ""));
                }
                break;
            case "INSERT":
                queryParams.setTipoQuery(QueryElements.QUERY_TYPE_INSERT);
                queryParams.addNomeTabella(split.get(2));
                int indexValues = split.indexOf("VALUES");
                for (int i = 3; i < indexValues; i++) {
                    queryParams.addCampo(split.get(i).replaceAll("[,()]", ""));
                }
                break;
            case "DELETE":
                queryParams.setTipoQuery(QueryElements.QUERY_TYPE_DELETE);
                queryParams.addNomeTabella(split.get(2)+"."+split.get(3));
                break;
        }
        addConditionsCampiVal(queryParams, split, indexWhere);
        queryParams.setOrderBy("something");
        System.out.println(queryParams);
        queryGenerator( queryParams );

    }

    public static void queryGeneratorLazyJoin(QueryParams queryParams, List<String> queryTotale, int indexInizio, int indexFine) {
        for (int i = indexInizio; i < indexFine; i++) {

            switch (queryTotale.get(i)) {
                case "JOIN":
                    if (!List.of("INNER", "LEFT", "RIGHT").contains(queryTotale.get(i - 1))) {
                        queryParams.addNomeTabella(queryTotale.get(i + 1) + "." + queryTotale.get(i + 2) + ".J");
                        addCampiValTab(queryParams, queryTotale, i, 0);
                    }
                    break;
                case "INNER":
                    queryParams.addNomeTabella(queryTotale.get(i + 2) + "." + queryTotale.get(i + 3) + ".IJ");
                    addCampiValTab(queryParams, queryTotale, i, 1);
                    break;
                case "LEFT":
                    queryParams.addNomeTabella(queryTotale.get(i + 2) + "." + queryTotale.get(i + 3) + ".LJ");
                    addCampiValTab(queryParams, queryTotale, i, 1);
                    break;
                case "RIGHT":
                    queryParams.addNomeTabella(queryTotale.get(i + 2) + "." + queryTotale.get(i + 3) + ".RJ");
                    addCampiValTab(queryParams, queryTotale, i, 1);
                    break;

            }
        }
    }
    public static void addCampiValTab (QueryParams queryParams, List<String> queryTotale, int indexInizio, int joinindex) {
        indexInizio = indexInizio + joinindex;
        queryParams.getCampiIdentificativiTabelle().add(queryTotale.get(indexInizio + 4));
        queryParams.getValoriIdentificativiTabelle().add(queryTotale.get(indexInizio + 6));
        int adding = 7;
        String check = queryTotale.get(indexInizio + joinindex + adding);
        String check1 = queryTotale.get(indexInizio);

        String check2 = queryTotale.get(indexInizio + adding);

        while (queryTotale.size()-1 >= (indexInizio + adding) && queryTotale.get(indexInizio + adding).equals("AND")) {
            queryParams.getCampiIdentificativiTabelle().add(queryTotale.get(indexInizio  + adding + 1 ));
            queryParams.getValoriIdentificativiTabelle().add(queryTotale.get(indexInizio + adding + 3) );
            adding = adding + 4;
        }
    }
    public static void addConditionsCampiVal (QueryParams queryParams, List<String> queryTotale, int indexWhere) {
        int adding = 0;
        queryParams.getCampiIdentificativi().add(queryTotale.get((indexWhere + 1) + adding));
        if (!queryTotale.get((indexWhere + 3) + adding).contains(":")) {
            queryParams.getValoriIdentificativi().add(queryTotale.get((indexWhere + 3) + adding));
        }
        adding = adding + 4;
        while (queryTotale.size()-1 >= (indexWhere + adding) && queryTotale.get(indexWhere + adding).equals("AND")) {
            queryParams.getCampiIdentificativi().add(queryTotale.get((indexWhere + 1) + adding));
            if (!queryTotale.get((indexWhere + 3) + adding).contains(":")) {
                queryParams.getValoriIdentificativi().add(queryTotale.get((indexWhere + 3) + adding));
            }
            adding = adding + 4;
        }
    }

    public static void queryGeneratorLazyNoAlarmStillInBed(String queryInput, QueryParams queryParams, LinkedHashMap<String, String> listaordinata, Map<String, OggettoValori> mappaLazy) {
        String modified = queryInput.replaceAll("\"([^\"]*)\"", "\"$1\"")
                .replaceAll("'([^']*)'", "'$1'")
                .replaceAll("[\\n+\\s]", " ");
        String[] splitArray = modified.trim().split("\\s+");
        List<String> split = Arrays.asList(splitArray);
        for (String element: split) {
            if (element.contains(":")) {
                queryParams.addCampoLazy(element.replace(":", ""));
            }
        }
        Map<Mykey, String> mapParameterSource = new HashMap<>();
        for (String campo: queryParams.getCampiLazy()) {
            Map.Entry<String, String> elemento = listaordinata.entrySet().iterator().next();
            if (listaordinata != null) {
                mapParameterSource.put(new Mykey(campo, convertType(elemento.getValue(), elemento.getKey()), checkType(elemento.getValue())), "");

            } else if (mappaLazy != null) {
                mapParameterSource.put(new Mykey(campo,
                        convertType(mappaLazy.get(campo).getValore(),
                                mappaLazy.get(campo).getValore()), checkType(mappaLazy.get(campo).getType())), "");

            }

        }
        System.out.println(queryInput);
        for (Mykey key: mapParameterSource.keySet()) {
            System.out.println("mapParam    key:    " + key.key1);
        }


        }


}
