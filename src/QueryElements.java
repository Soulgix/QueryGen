public class QueryElements {

    public static final String QUERY_TYPE_SELECT = "SELECT";
    public static final String QUERY_TYPE_DELETE = "DELETE";

    public static final String QUERY_TYPE_INSERT = "INSERT";

    public static final String QUERY_TYPE_UPDATE = "UPDATE";

    public static final String Type_NUMERIC = "NUMBER";

    public static final String Type_VARCHAR = "VARCHAR";

    public static final String Type_TIMESTAMP = "TIMESTAMP";
}
